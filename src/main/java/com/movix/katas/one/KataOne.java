package com.movix.katas.one;

import com.movix.katas.one.exception.NoValidMsisdnException;

public class KataOne {

	public String processCommand(String msisdn, String command) throws NoValidMsisdnException {
		if (msisdn != null && command != null && command.length() > 0) {
			if (msisdn.length() != 11) {
				throw new NoValidMsisdnException();
			}
			String msg = getMsg(command);
			if (msg != null) {
				return msg;
			}
			msg = gift(command);
			if (msg != null) {
				return msg;
			}
			return "Comando desconocido. Envie \nayuda\n para ver los comandos disponibles";
		} else if (msisdn == null) {
			throw new NoValidMsisdnException();
		}
		return null;
	}

	/**
	 * Retorna un mensaje de salida, dado un comando
	 * 
	 * @param command
	 *            un comando a procesar
	 * @return el mensaje de salida
	 * 
	 * @author Rodrigo Vergara P. (rvergarap@gmail.com)
	 */
	private String getMsg(String command) {
		String commandLower = command.toLowerCase();
		String msg = null;
		switch (commandLower) {
		case "echo":
			msg = command;
			break;
		case "juegos":
		case "horoscopo":
			msg = "Bienvenido. Ya te encuentras suscrito a " + command.toUpperCase();
			break;
		case "ayuda":
			msg = "Envia \"echo\" para probar el servicio. Envia \"juegos\" para suscribirte a Juegos.";
			break;
		}
		if (commandLower.startsWith("salir ")) {
			String service = commandLower.replace("salir ", "");
			service = service.trim();
			if (service.equals("juegos") || service.equals("horoscopo")) {
				msg = "Te has desuscrito de " + service.toUpperCase();
			}
		}
		return msg;
	}

	/**
	 * Retorna un mensaje de salida, correspondiente a un reagalo de paquete de
	 * datos, dado un comando
	 * 
	 * @param command
	 *            un comando a procesar
	 * @return el mensaje de salida
	 * 
	 * @author Rodrigo Vergara P. (rvergarap@gmail.com)
	 */
	private String gift(String command) {
		String msg = null;
		int Command_Length = command.length();
		if ((Command_Length == 9 || Command_Length == 11)) {
			// command could be a msisdn, so we must send a gift
			try {
				Long.parseLong(command);
				// command is a msisdn. we need to send a gift. we need to
				// include the msisdn but in local format
				String msisdn2 = (command.length() == 11) ? command.substring(2) : command;
				msg = "Has regalado un paquete de datos a tu amig@ " + msisdn2;
			} catch (NumberFormatException e) {
				// nothing to do
			}
		}
		return msg;
	}
}
